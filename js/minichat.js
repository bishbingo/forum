var
    ok = true,
    upTime = 5,
    maxLine = 150,
    uc = window.uc || {},
    ca = document.write('<div class="uchat"><div class="ut"><\/div><div class="uc"><\/div><div class="ub"><\/div><\/div>');
(uc = {
    bbc: function (e) {
        if (e !== null) {
            return "parent.window.document.getElementById('mchatMsgF').focus();parent.window.document.getElementById('mchatMsgF').value+='" + e + "';return false;";
        }
    },
    ucodes_get: function () {
        $.get('/mchat', function (o) {
            try {
                $('.uc').html('');
            }
            catch (e) {
                _uWnd.alert('<font color="red">Ошибка приема данных!</font>', '', {w: 200, h: 50, pad: '5px'});
            }
            finally {
                $('div[class*="cBlock"]', o).each(function (i) {
                    $('.ut').html('Всего сообщений - ' + (i + 1));
                    var ucodes_username = "'" + $('b', this).html() + "'";
                    $('.uc').append('<table cellpadding="0" cellspacing="0" width="100%"><tr><td class="user" colspan="2" align="left" valign="top"><img src="/img/chat.png" onclick="return uc.showProfile(' + ucodes_username + ')" align="left" \/>&nbsp;<a href="javascript:void(Apply%20to)" onclick="' + uc.bbc('[i]' + $('b', this).html() + '[/i], ') + '">' + $('b', this).html() + '<\/a><\/td><td class="data" colspan="2" align="right" valign="top">' + $('div', this).html() + '<\/td><\/tr><tr><td class="message" colspan="4" align="left" valign="top">' + $('.cMessage', this).html() + '<\/td><\/tr><\/table>');
                });
                setTimeout('uc.ucodes_get()', upTime * 1000);

                if ($('.uc').html() == '') {
                    $('.uc').html('<div style="padding:5px;color:#FFF;">Сообщений нет.<\/div>');
                    $('.ut').html('Всего сообщений - 0');
                }
            }
        });
    },
    ucodes_post: function () {
        if (ok == true) {
            if ($('.mes').val() == '') {
                _uWnd.alert('<font color="red">Введите текст сообщения!</font>', '', {w: 200, h: 50, pad: '5px'});
                return false;
            } else {
                var ref = Math.random() * (10000 * 99999);
                _uPostForm('uCaddFrm', {
                    type: 'POST',
                    url: '/mchat/?' + ref,
                    error: function () {
                        _uWnd.alert('<font color="red">Ошибка отправки данных!</font>', '', {
                            w: 200,
                            h: 50,
                            pad: '5px'
                        });
                    },
                    success: function () {
                        _uWnd.alert('Сообщение успешно добавлено!', '', {w: 200, h: 70, pad: '5px'})
                        $('.mes').val('');
                        uc.ucodes_get();
                    },
                });
            }
        }
    },
    showProfile: function (user) {
        window.open('/index/8-0-' + user);
    },
    cnt: function () {
        var
            i,
            a = $('.mes').val(),
            rst = maxLine - $('.mes').val().length,
            b = '..ru|..ua|..tv|..tk|..in|..kz|..su|..org|..net|..com|..biz|..inf|..info|..name|..рф|_http:|http:|hxxp|ftp|mss|порно|сука|бля|ебан|мудак|мудила|троль|тролль|пидор'.split('|');
        for (i = 0; i < b.length; ++i) {
            var bad = new RegExp(b[i], 'gi');
            if (a.search(bad) != -1) {
                $('.mes').val('');
                _uWnd.alert('<center><font color="red"><b>Внимание!</b><hr>Спам и мат запрещены в сообщениях!</font>', '', {
                    w: 200,
                    h: 90,
                    pad: '5px'
                });
                ok = false;
            } else {
                ok = true;
            }
        }
        if (rst < 0) {
            rst = 0;
            $('.mes').val($('.mes').val().substr(0, maxLine));
        }
        $('.num').html(rst);
    },
    frm: function (user) {
        var c, g,
            ms = "window.open('/mchat/0-1','mchatCtrl','scrollbars=1,width=550,height=550,left=0,top=0'); return false;",
            sm = "new _uWnd('Sml','-',-250,-350,{autosize:0,closeonesc:1,resize:0},{url:'/index/35-1-2'}); return false;";
        if (user !== null) {
            if (user == 0) {
                maxLine = 50;
                $('.ub').html('<span style="text-align:center; font-weight:bold; color:#fff;font-size:11px;padding:5px;"><br>Чат доступен только пользователям сайта.<a href="/index/3"><br>Зарегистрироваться<\/a><br><br><\/span>');
            } else if (user >= 1) {
                $('.ub').html('\
                                  <form style="margin:0;padding:0;" id="uCaddFrm" onsubmit="ucodes_post(); rerurn false;">\
                                  <table cellpadding="0" cellspacing="0" width="100%">\
                                  <tr>\
                                  <td class="addcom" colspan="2" align="left" valign="top">\
                                  <div style="float:left;"><img src="/img/smile.gif" onclick="' + uc.bbc(' :) ') + '" /><img src="/img/sad.gif" onclick="' + uc.bbc(' :( ') + '" /><img src="/img/lol.gif" onclick="' + uc.bbc(' :lol: ') + '" /><img src="/img/blink.gif" onclick="' + uc.bbc(' :blink: ') + '" />\
                        <\/div>\
                        <div style="float:right"><img src="/img/allsmile.png" onclick="' + sm + '" /><img src="/img/tools.png" onclick="' + ms + '" />\
                        <\/div>\
                        <\/td>\
                        <td class="addcom" colspan="2" align="right" valign="top"><span class="num">' + maxLine + '</span><\/td>\
                        <\/tr>\
                        <tr>\
                        <td class="comform" colspan="2" align="left" valign="top"><textarea name="mcmessage" class="mes" id="mchatMsgF" onkeyup="uc.cnt();" onfocus="uc.cnt();"></textarea><\/td>\
                        <td class="comform" colspan="2" align="right" valign="top"><input type="button" onclick="uc.ucodes_post(); return false;" value="OK" /><input type="hidden" name="numa" id="numa" value="0"><input type="hidden" name="a" value="18"><input type="hidden" id="ajaxFlag" name="ajax" value="1" /><\/td>\
                        <\/tr>\
                        <\/table>\
                        <\/form>\
                        ');
            }
            $('input').hover(
                function () {
                    if ($(this).attr('type') == 'text') {
                        c = $(this).val();
                        $(this).focus(function () {
                            $(this).val('');
                            def = $(this).val()
                        })
                    }
                },
                function () {
                    if ($(this).attr('type') == 'text') {
                        $(this).blur(function () {
                            if ($(this).val() == def)
                                $(this).val(c);
                            def = c;
                        })
                    }
                });
        }
    },
    author: function () {
        new _uWnd('au', 'О Скрипте', 300, 100, {modal: 1}, '<center style="padding:5px;"><\/a><\/center>');
    },
    initialize: function (User, upDate, maxLines) {
        if (User >= null) {
            if (upDate)
                upTime = upDate;
            if (maxLines)
                maxLine = maxLines;
            uc.frm(User);
            uc.ucodes_get();
        } else {
            $('.uchat').remove();
            _uWnd.alert('<font color="#CC0000">Ошибка инициализации скрипта!<br>Впишите параметры в функцию инициализации<\/font>  <b>initialize!<\/b>', '', {
                w: 200,
                h: 75,
                pad: '5px'
            });
        }
    }
})(uc);
